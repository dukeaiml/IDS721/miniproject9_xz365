# mini-project9: Streamlit App with a Hugging Face Model
> Xingyu, Zhang (NetID: xz365)

## Hugging Face LLM Chatbot

This Streamlit application is a chatbot powered by an open-source language model from Hugging Face. Users can input prompts, and the chatbot generates text based on those prompts using the GPT-2 model.


## Setup

Follow these steps to set up the environment and deploy the application:

1. **Install Dependencies**
    Ensure python is installed correctly in the computer and activate a vertual environment using the command:

    ```bash
    conda activate your-env-name
    ```
    Then, install the required Python packages included in [`requirement.txt`](./requirements.txt) using pip:

    ```bash
    pip install -r requirements.txt
    ```

2. **Develop Function In [app.py](./app.py)**
```python
import streamlit as st
from transformers import pipeline

model_name = "gpt2"
text_generator = pipeline("text-generation", model=model_name)

def main():
    st.title("Hugging Face LLM Chatbot")

    st.write(
        "Welcome to the Hugging Face LLM Chatbot! Enter a prompt below and see how the model responds."
    )

    prompt = st.text_input("Enter your prompt here:", "")

    if st.button("Generate"):
        if prompt:
            generated_text = text_generator(prompt, max_length=150, do_sample=True)[0][
                "generated_text"
            ]
            st.write("Generated Text:")
            st.write(generated_text)
        else:
            st.error("Please enter a prompt.")

if __name__ == "__main__":
    main()
```

3. **Run the Application Locally**
   Execute the following command to run the Streamlit app locally:
   ```bash
   streamlit run app.py
   ```
   This will start a local server, and you can access the app in your web browser at http://localhost:8501.


4. **Deploy on Streamlit Cloud**
   - Sign in on [Streamlit Cloud](https://streamlit.io/) (sign up first if you don't have an account yet).
   - Upload the project on [Github](https://github.com/ireneisme/721_miniproject9) and deploy it on the Streamlit Cloud.
   - Check the deployed app at https://721miniproject9-pgaui8ciqxeetjruqgnxpv.streamlit.app/
  
![app](./assets/app.png)

   - Check the function:
![result](./assets/cloudDeploy.png)

*[`demo`](./demo.mkv) video is included in the project*